using Microsoft.EntityFrameworkCore;
using PositiveDriveCMS.EF.Models;

namespace PositiveDriveCMS.EF 
{
    public class PoiDbContext : DbContext
    {
        public DbSet<Models.POI> Poi { get; set; }

        public PoiDbContext() 
        {
        }
        
        public PoiDbContext(DbContextOptions<PoiDbContext> options) : base(options) 
        { 
        }
    }
}
