using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PositiveDriveCMS.EF.Models
{
    public class POI
    {
        public string Id { get; set; }
        public string Description { get; set; }

        [Column("smiles")]
        public double _smiles {get; set; }
        public int Smiles => (int)_smiles;
        
        public double Longitude { get; set; }
        public double Latitude { get; set; }

        public override string ToString()
        {
            return $"Id = {Id}, Description = {Description}, Smiles = {Smiles}, Longitude = {Longitude}, Latitude = {Latitude}\n";
        }
    }
}
