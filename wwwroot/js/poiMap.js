function showPoisOnMap(poisUrl)
{
    var map;
    
    GoogleMapsLoader.KEY = "AIzaSyCGQ454QJ7w77dlIQaDXBp2UpXbzyINQPM";
    GoogleMapsLoader.load(init);

    GoogleMapsLoader.onLoad((google) => {
        getPois();
    });

    function init(google) {
        map = new google.maps.Map(document.getElementById('map'), {
            center: { lat: -34.397, lng: 150.644 },
            animation: google.maps.Animation.DROP,
            zoom: 8
        });
    }

    function getPois() {
        $.getJSON(poisUrl, (pois) => {
            pois.forEach(addMarker);
        });
    }

    function getInfoText(poi) {
        return `<div id="content">
                <div id="siteNotice">
                </div>
                <h1 id="firstHeading" class="firstHeading">${poi.smiles | 0} no. of :)</h1>
                <div id="bodyContent">
                    ${poi.description | "No description"}
                </div>
            </div>`;
    }

    function onMarkerClick(marker, poi) {
        var infowindow = new google.maps.InfoWindow({
            content: getInfoText(poi)
        });
        infowindow.open(map, marker);
    }

    function addMarker(poi) {
        var marker = new google.maps.Marker({
            position: { lat: poi.latitude, lng: poi.longitude },
            map: map,
            title: `${poi.smiles}`,
        });

        marker.addListener('click', onMarkerClick.bind(null, marker, poi));
    }
}