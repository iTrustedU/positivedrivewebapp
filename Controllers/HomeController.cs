using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace PositiveDriveCMS.Controllers
{
    public class HomeController : Controller
    {
        private readonly EF.PoiDbContext _context;

        public HomeController(EF.PoiDbContext poiContext)
        {
            _context = poiContext;
        }
        
        public IActionResult Pois()
        {
            return Json(_context.Poi.ToList());
        }

        public IActionResult Index()
        {
            return View();
        }
        
        public IActionResult Error() 
        {
            return View();
        }
    }
}
